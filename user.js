// ==UserScript==
// @name         Msy calcs
// @namespace    http://your.homepage/
// @version      0.1
// @description  enter something useful
// @author       Chris Chase <tampermonkey@sirmonkeys.com>
// @match        http://www.msy.com.au/saonline/*
// @grant        GM_addStyle
// ==/UserScript==

Array.prototype.clean = function(deleteValue) {
    for (var i = 0; i < this.length; i++) {
        if (this[i] == deleteValue) {         
            this.splice(i, 1);
            i--;
        }
    }
    return this;
};

console.log("My Msy calcs");

GM_addStyle(".results{ width:90%; border:1px solid black; margin:10px auto;} ");
GM_addStyle(".resDiv{ width:100%; border:1px solid grey; margin:10px auto; padding:2px;    text-align: center;} ");
GM_addStyle(".results tr td{ padding-left:3px} ");
GM_addStyle(".results tr th{ padding-left:3px; border-bottom:1px solid black;} ");
GM_addStyle(".CalculateButton {width:50%;  padding:5px 25px;         display: inline-block;} ");
GM_addStyle(".aveSpan {width:50%; margin:auto; padding:5px 25px;     display: block; text-align:center} ");

var res = $("<div/>").addClass("resDiv");
var avespan = $("<span/>").addClass("aveSpan");
var table = $("<table/>").addClass("results");
var unknowns = $("<div/>");
var bt = $("<button class='CalculateButton'>Calculate Per TB Costs</button>");
var bt2 = $("<button class='CalculateButton'>Calculate Per GB Costs</button>");

//Magic Consts
var GBPerTB = 1000;// 1024 ;)


function getSize(strstring, GBPerTB, ResINTB){
    isGB = false;
    size= 0;
    sizeArr = strstring.split("TB").clean("");
    if (sizeArr.length<=1){
        isGB = true;
        sizeArr =strstring.split("GB").clean("");
    }  
    if (sizeArr.length<=1){
        isGB = true;
        sizeArr = strstring.split("G ").clean("");
    }  

    if (sizeArr.length<=1){
        //watts still working it out
        sizeArr = strstring.split("W").clean("");
    }  


    if (sizeArr.length>1){
        sizeArr = sizeArr[0].split(" " );
        sizeStr = sizeArr[sizeArr.length -1];

        if (sizeStr ==Math.round(sizeStr)) {
            size = Math.round(sizeStr);
            if (ResINTB && isGB){
                size = size / GBPerTB;
            }
            if (!ResINTB && !isGB){
                size = size * GBPerTB;
            }
        }
    }  


    return size;
}

function Calc(ResINTB){
    var sizeunit = "TB";
    if (!ResINTB) {
        sizeunit = "GB";
    }

    var ave = 0;
    unknowns.html("");
    table.html("");
    table.append("<tr><th>$ Per " + sizeunit + "</th><th>Total " + sizeunit + " Size</th> <th>Total Price</th> <th>Desc</th></tr>");

    $('.bottom_block').each(function() {
        a = $(this).children('h3').children('a');
        size = getSize(a.html(), GBPerTB, ResINTB);



        price = $(this).children('.content_price').children('.price').html();
        while(price.charAt(0) === '$')
            price = price.substr(1);

        linka = "<a href='"+ a.attr('href')+"'>" + a.html() + "</a>";

        if (size > 0){
            priceper = (price/size);
            if (ave === 0 ) {
                ave = priceper;
            } else {
                ave = ( ave + priceper ) /2;
            }

            table.append("<tr><td>$" +  Math.round (priceper*100)/100  + "</td><td>" + Math.round(size*100)/100  + "</td> <td>$" + price+ "</td> <td>"+linka+"</td></tr>");
        } else {

            unknowns.append("<p>Unknown Size: $" + price + ", " + linka + "</p>");
        }


    });



    if (ResINTB) {
        avespan.html("Ave Cost Per TB: $"+Math.round ((ave*100)/100 ) +  " <br/>Ave Cost Per GB: $"+Math.round ((ave/GBPerTB*100)/100 ));

    } else {
        avespan.html("Ave Cost Per TB: $"+Math.round ((ave*100*GBPerTB)/100 ) +  " <br/>Ave Cost Per GB: $"+Math.round ((ave*100)/100 ));
    }



}


bt.click(function() {   Calc(true); });
bt2.click(function() {  Calc(false); });

res.append(table);
res.append(unknowns);
res.prepend(avespan);
res.prepend(bt);
res.prepend(bt2);

res.append("<i><b>Notice:</b> 1 TB = " + GBPerTB + " GB </i>");
$("#center_column").prepend(res);
